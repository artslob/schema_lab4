.global entry

.data   /* RAM = 0x200-0x3FF */
/* 0x200 */ .word 0x0000 
/* 0x201 */ .word 0x0001 
/* 0x202 */ .word 0x0002 
/* 0x203 */ .word 0x0003 
/* 0x204 */ .word 0x0004
/* 0x205 */ .word 0x8000F00F /* value for operations */
/* 0x206 */ .word 0xFFFF
/* 0x207 */ .word 0x0000 /* value for previous ex_op */      
.text                  
.ent entry

/* ioctrl = 0x800, gpio = 0x400 */
/* 
	0x400 = op[2]
	0x402 = offset[5]
 */
entry:	
		init:
		lw $t4, 0x200
		lw $t5, 0x201
		lw $t6, 0x202
		lw $t7, 0x205
		
		start:
		
		lw $t2, 0x400 /* op */
		lw $t3, 0x402 /* offset */
		
		beq $t2, $t4, make_sll
		beq $t2, $t5, make_srl
		beq $t2, $t6, make_sra
		sw $t3, 0x400
		j start
		
		make_sll:
		sll $t1, $t7, $t3
		sw $t1, 0x400
		j start
		
		make_srl:
		srl $t1, $t7, $t3
		sw $t1, 0x400
		j start
		
		make_sra:
		sra $t1, $t7, $t3
		sw $t1, 0x400
		j start
		
		
		j entry
.end entry
